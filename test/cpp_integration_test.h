//
// Created by dmanik on 29.11.20.
//

#ifndef RIDEPY_CPP_INTEGRATION_TEST_H
#define RIDEPY_CPP_INTEGRATION_TEST_H

#include <algorithm> // for max()
#include <cmath>
#include <tuple>
#include <utility> // for pair
#include <vector>
//#include <boost/foreach.hpp>
//#include <boost/range/iterator_range.hpp>
#include "../ridepy/data_structures_cython/cdata_structures.h"
#include "../ridepy/util/dispatchers_cython/cdispatchers.h"
#include "../ridepy/util/spaces_cython/cspaces.h"
#include "../ridepy/vehicle_state_cython/cvehicle_state.h"
#include <chrono> // for benchmarking
#include <iostream>
#include <random>

using namespace std;

#endif
// RIDEPY_CPP_INTEGRATION_TEST_H
