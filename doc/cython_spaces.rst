Cython Spaces
~~~~~~~~~~~~~
.. warning::
   Add text describing the inheritance structure, and the need to hold an
   union.

.. automodule:: ridepy.util.spaces_cython
    :members: TransportSpace


.. automodule:: ridepy.util.spaces_cython
    :members: TransportSpace, Euclidean2D, Manhattan2D

.. warning::
   Add text describing how the graphs can only hold integers as nodes and how
   we automatically relabel the nodes.

.. automodule:: ridepy.util.spaces_cython
    :members: Graph


