Module ``util``
===============
The module contains some utility functions and classes.


..
    TODO More manual docs in between the autodocs



.. include:: ./dispatchers.rst

.. include:: ./spaces.rst

The request generators
----------------------

.. automodule:: ridepy.util.request_generators
    :members:

The analytics module
--------------------

.. automodule:: ridepy.util.analytics
    :members:


